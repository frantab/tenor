# tenor app

Tenor app for Košík.cz

Public app: https://tenorprokosik.cz/

## Task
* Aplikace má vypisovat obsah z rest API https://tenor.com/gifapi/documentation.
* Používá Vue router a Vuex.
* Má minimálně dvě stránky.
* První stránka výpis trendů.
* Druhá stránka výsledky vyhledávání.
* Výraz pro vyhledávání se píše do inputu. Request na API se automaticky odešle, pokud uživatel přestane psát na více jak 200ms.
* Obsah z API se ukládá do Vuexu, odkud se data vypisují do komponent.
* Výsledná aplikace je uložena ve veřejném git repozitáři.

## Init
1. `yarn`
2. `cp .env.example .env` and replace `REPLACE_FOR_KEY` for your tenor api key (https://developers.google.com/tenor/guides/quickstart)
3. `yarn dev` and enjoy

----------

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
    1) Run `Extensions: Show Built-in Extensions` from VSCode's command palette
    2) Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
yarn
```

### Compile and Hot-Reload for Development

```sh
yarn dev
```

### Type-Check, Compile and Minify for Production

```sh
yarn build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
yarn test:unit
```

### Run End-to-End Tests with [Cypress](https://www.cypress.io/)

```sh
yarn build
yarn test:e2e # or `npm run test:e2e:ci` for headless testing
```

### Lint with [ESLint](https://eslint.org/)

```sh
yarn lint
```
