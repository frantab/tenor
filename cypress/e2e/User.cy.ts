describe('User', () => {
  it('visits the app root url', () => {
    cy.visit('/');
    cy.contains('h1', 'Gifs');
  });

  it('visits the about url', () => {
    cy.visit('/about');
    cy.contains('h1', 'About');
  });

  it('visits the not existing url', () => {
    cy.visit('/test');
    cy.contains('h1', 'Not found');
  });
});
