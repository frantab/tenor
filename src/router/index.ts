import { createRouter, createWebHistory, type RouteRecordRaw } from 'vue-router';

export enum Routes {
  Gifs = 'Gifs',
  About = 'About',
}

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: Routes.Gifs,
    component: () => import(/* webpackChunkName: "tenors" */ '../views/Gifs.vue'),
  },
  {
    path: '/gifs',
    redirect: { name: Routes.Gifs },
  },
  {
    path: '/about',
    name: Routes.About,
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
  {
    path: '/:pathMatch(.*)*',
    component: () => import(/* webpackChunkName: "not-found" */ '../views/NotFound.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

export default router;
