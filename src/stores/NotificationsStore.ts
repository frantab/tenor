import { defineStore } from 'pinia';
import { v4 as uuidv4 } from 'uuid';
import { reactive } from 'vue';

export enum NotificationTypes {
  Info = 'info',
  Warning = 'warning',
  Error = 'error',
}

export interface Notification {
  id: string;
  title: string;
  message: string;
  timeout: number; // time in ms
  type: NotificationTypes;
  timeLeft: number;
}

export interface NotificationCreateParams extends Partial<Omit<Notification, 'timeLeft'>> {
  title: string;
  message: string;
}

export interface NotificationsStoreState {
  notifications: Notification[];
}

export const useNotificationsStore = defineStore('NotificationsStore', {
  state: (): NotificationsStoreState => ({
    notifications: [],
  }),
  actions: {
    removeNotification(id: string) {
      const notificationToBeRemovedIndex = this.notifications.findIndex((notification) => notification.id === id);

      if (notificationToBeRemovedIndex !== -1) this.notifications.splice(notificationToBeRemovedIndex, 1);
    },
    addNotification({
      id = uuidv4(),
      timeout = 5000,
      type = NotificationTypes.Info,
      ...notification
    }: NotificationCreateParams) {
      const newNotification: Notification = reactive({
        id,
        timeout,
        type,
        timeLeft: timeout,
        ...notification,
      });

      this.notifications.unshift(newNotification);

      // Set timout for auto remove (if timeout is 0, keep it)
      if (timeout > 0) setTimeout(() => this.removeNotification(newNotification.id), newNotification.timeout);

      // Set interval and sub 1000ms each 1000ms. timeLeft is used for animated fade.
      const interval = setInterval(() => {
        const notificationIndex = this.notifications.findIndex(({ id: notificationId }) => notificationId === id);

        if (notificationIndex !== -1) this.notifications[notificationIndex].timeLeft -= 1000;
        if (this.notifications[notificationIndex].timeLeft <= 1000) clearInterval(interval);
      }, 1000);
    },
  },
});
