import { defineStore } from 'pinia';
import { enableAnalytics, app } from '@/firebase';

export interface CookiesConsentStoreState {
  consent: boolean;
  wasPopupShowed: boolean;
}

export const useCookiesConsentStore = defineStore('CookiesConsentStore', {
  state: (): CookiesConsentStoreState => ({
    consent: localStorage.getItem('cookies_consent') === 'true',
    // Show when consent is not stored in local storage
    wasPopupShowed: localStorage.getItem('cookies_consent') !== null,
  }),
  actions: {
    init() {
      if (this.consent) this.enable();
    },

    setConsent(value: boolean) {
      this.consent = value;
      localStorage.setItem('cookies_consent', `${value}`);
      app.automaticDataCollectionEnabled = value;
    },

    enable() {
      this.setConsent(true);
      enableAnalytics();
      this.wasPopupShowed = true;
    },

    disable() {
      this.setConsent(false);
      this.wasPopupShowed = true;
    },
  },
});
