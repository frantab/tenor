import { defineStore } from 'pinia';
import {
  getFeaturedTenors,
  getSearchSuggestions,
  getTrendingSearchTerms,
  searchTenors,
  TenorArRange,
  TenorContentFilters,
  TenorMediaFilters,
  type TenorGif,
} from '@/services/tenor';
import { NotificationTypes, useNotificationsStore } from './NotificationsStore';

export interface TenorStoreState {
  tenors: TenorGif[];
  randomTenor: TenorGif | null;
  searchText: string;
  searchSuggestions: string[];
  trendingSearchTerms: string[];
  loadingCounter: number;
}

const TENOR_API_KEY = `${import.meta.env.VITE_TENOR_API_KEY}`;

export const useTenorStore = defineStore('TenorStore', {
  state: (): TenorStoreState => ({
    tenors: [],
    randomTenor: null,
    searchText: '',
    searchSuggestions: [],
    trendingSearchTerms: [],
    loadingCounter: 0,
  }),
  getters: {
    loading(state) {
      return Boolean(state.loadingCounter);
    },
  },
  actions: {
    async getTenors(random = false) {
      this.loadingCounter += 1;

      try {
        // Get searched tenors if there is q, get featured otherwise
        const { data: { results } } = this.searchText
          ? await searchTenors({
            key: TENOR_API_KEY,
            q: this.searchText,
            country: 'CZ',
            locale: 'cs_CZ',
            contentfilter: TenorContentFilters.Off,
            media_filter: TenorMediaFilters.Basic,
            ar_range: TenorArRange.All,
            limit: 10,
            random,
          })
          : await getFeaturedTenors({
            key: TENOR_API_KEY,
            country: 'CZ',
            locale: 'cs_CZ',
            contentfilter: TenorContentFilters.Off,
            media_filter: TenorMediaFilters.Basic,
            ar_range: TenorArRange.All,
            limit: 10,
          });

        this.tenors = results;
      } catch (_) {
        const notificationsStore = useNotificationsStore();

        notificationsStore.addNotification({
          title: 'Error',
          message: 'Nastala chyba při získávání gifů',
          type: NotificationTypes.Error,
        });
      }

      this.loadingCounter -= 1;
    },

    async getRandomTenor(q: string) {
      this.loadingCounter += 1;

      try {
        const { data: { results } } = await searchTenors({
          key: TENOR_API_KEY,
          q,
          country: 'CZ',
          locale: 'cs_CZ',
          contentfilter: TenorContentFilters.Off,
          media_filter: TenorMediaFilters.Basic,
          ar_range: TenorArRange.All,
          random: true,
          limit: 1,
        });

        this.randomTenor = results?.[0] || null;
      } catch (_) {
        const notificationsStore = useNotificationsStore();

        notificationsStore.addNotification({
          title: 'Error',
          message: 'Nastala chyba při získávání náhodného gifu',
          type: NotificationTypes.Error,
        });
      }

      this.loadingCounter -= 1;
    },

    async getSearchSuggestions() {
      if (this.searchText) {
        this.loadingCounter += 1;

        try {
          // Get searched tenors if there is q, get featured otherwise
          const { data: { results } } = await getSearchSuggestions({
            key: TENOR_API_KEY,
            q: this.searchText,
            country: 'CZ',
            locale: 'cs_CZ',
            limit: 5,
          });

          this.searchSuggestions = results;
        } catch (_) {
          const notificationsStore = useNotificationsStore();

          notificationsStore.addNotification({
            title: 'Error',
            message: 'Nastala chyba při získávání návrhů vyhledávání',
            type: NotificationTypes.Error,
          });
        }

        this.loadingCounter -= 1;
      } else {
        this.searchSuggestions = [];
      }
    },

    async getTrendingSearchTerms() {
      if (!this.trendingSearchTerms.length) {
        this.loadingCounter += 1;

        try {
          // Get searched tenors if there is q, get featured otherwise
          const { data: { results } } = await getTrendingSearchTerms({
            key: TENOR_API_KEY,
            country: 'CZ',
            locale: 'cs_CZ',
            limit: 5,
          });

          this.trendingSearchTerms = results;
        } catch (_) {
          const notificationsStore = useNotificationsStore();

          notificationsStore.addNotification({
            title: 'Error',
            message: 'Nastala chyba při získávání trendů vyhledávání',
            type: NotificationTypes.Error,
          });
        }

        this.loadingCounter -= 1;
      }
    },
  },
});
