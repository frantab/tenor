import axios from 'axios';

export const tenorApi = axios.create({
  baseURL: 'https://tenor.googleapis.com',
});
