import type { AxiosResponse } from 'axios';
import { tenorApi } from './api';

export enum TenorContentFilters {
  Off = 'off',
  Low = 'low',
  Medium = 'medium',
  High = 'high',
}

export enum TenorMediaFilters {
  Basic = 'basic',
  Minimal = 'minimal',
}

export enum TenorArRange {
  All = 'all',
  Wide = 'wide',
  Standard = 'standard',
}

export enum TenorGifFormats {
  Gif = 'gif',
  Gifpreview = 'gifpreview',
  Mp4 = 'mp4',
  Nanogif = 'nanogif',
  Nanogifpreview = 'nanogifpreview',
  Nanomp4 = 'nanomp4',
  Tinygif = 'tinygif',
  Tinygifpreview = 'tinygifpreview',
  Tinymp4 = 'tinymp4',
}

export interface TenorMediaObject {
  preview: string; // a url to a preview image of the media source
  url: string; // a url to the media source
  dims: number[]; // width and height in pixels
  size: number; // size of file in bytes
}

export interface TenorGif {
  created: number;
  hasaudio: boolean;
  id: string;
  media_formats: { [key in TenorGifFormats]: TenorMediaObject };
  tags: string[]
  title: string
  content_description: string
  itemurl: string
  hascaption: boolean
  flags: string
  bg_color: string
  url: string
}

export interface TenorResponse {
  next: string;
  results: TenorGif[];
}

// SEARCH
export interface TenorSearchParams {
  key: string;
  q: string;
  client_key?: string;
  searchfilter?: string;
  country?: string;
  locale?: string;
  contentfilter?: TenorContentFilters;
  media_filter?: TenorMediaFilters;
  ar_range?: TenorArRange;
  random?: boolean;
  limit?: number;
  pos?: string;
}

export const searchTenors = (params: TenorSearchParams): Promise<AxiosResponse<TenorResponse>> => tenorApi.get('/v2/search', { params });

// FEATURED
export interface TenorFeaturedParams {
  key: string;
  client_key?: string;
  searchfilter?: string;
  country?: string;
  locale?: string;
  contentfilter?: TenorContentFilters;
  media_filter?: TenorMediaFilters;
  ar_range?: TenorArRange;
  limit?: number;
  pos?: string;
}

export const getFeaturedTenors = (params: TenorFeaturedParams): Promise<AxiosResponse<TenorResponse>> => tenorApi.get('/v2/featured', { params });

// SEARCH SUGGESTIONS
export interface TenorSearchSuggestionsParams {
  key: string;
  q: string;
  client_key?: string;
  country?: string;
  locale?: string;
  limit?: number;
}

export interface TenorSearchSuggestionsResponse {
  results: string[];
}

export const getSearchSuggestions = (params: TenorSearchSuggestionsParams): Promise<AxiosResponse<TenorSearchSuggestionsResponse>> => tenorApi.get('/v2/search_suggestions', { params });

// TRENDING SEARCH TERMS
export interface TenorTrendingSearchTermsParams {
  key: string;
  client_key?: string;
  country?: string;
  locale?: string;
  limit?: number;
}

export interface TenorTrendingSearchTermsResponse {
  results: string[];
}

export const getTrendingSearchTerms = (params: TenorTrendingSearchTermsParams): Promise<AxiosResponse<TenorTrendingSearchTermsResponse>> => tenorApi.get('/v2/trending_terms', { params });
